import java.util.*;

public class Main {

    public static void main(String[] args) {
        initGraph();
        printGraph(graph);
//        printGraph(getTheMinimumSpanningTree());
        addEdgeToGraph(20); // NO change the graph
        printGraph(graph);
        //        printGraph(getTheMinimumSpanningTree());
        addEdgeToGraph(1); // CHANGE the graph
        getTheMinimumSpanningTree();
        printGraph(graphMST);

        // TODO
    }

    static class Graph {
        List<Node> nodes = new ArrayList<>();
    }

    static class Node {
        Map<Node, Edge> edges = new HashMap<>();
        int label;

        public Node(int label) {
            this.label = label;
        }
    }

    static class Edge {
        int weight;

        public Edge(int weight) {
            this.weight = weight;
        }
    }

    static Graph graph;
    static Graph graphMST = new Graph();
    static final int numOfNodes = 4;
    static final int numOfEdges = 3;
    static Random r = new Random();

    public static Graph getTheMinimumSpanningTree() {
        prim();
        return null;
    }

    public static void addEdgeToGraph(int weight) {
        Edge edge = new Edge(weight);
        System.out.println("After we put more edge (" + weight + "):");
        findNodesWithoutEdgeAndConnect(edge);

        // update minimumSpanningTree:
    }

    private static void initGraph() {
        graph = new Graph();
        initNodes();
        initEdges();
    }

    private static void initNodes() {
        for (int i=0; i < numOfNodes; i++) {
            Node node = new Node(i);
            graph.nodes.add(node);
        }
    }

    private static void initEdges() {
        for (int i=0; i < numOfEdges; i++) {
            Edge edge = new Edge(r.nextInt(6) + 5); // 5-10

            findNodesWithoutEdgeAndConnect(edge);
        }
    }

    private static void findNodesWithoutEdgeAndConnect(Edge newEdge) {
        // find two different nodes without connection for connecting to edge
        int srcIndex = r.nextInt(numOfNodes);
        int desIndex = r.nextInt(numOfNodes);
        Node srcNode = graph.nodes.get(srcIndex);
        Node desNode = graph.nodes.get(desIndex);

        while (desIndex == srcIndex || srcNode.edges.containsKey(desNode) ) {
            desIndex = r.nextInt(numOfNodes);
            desNode = graph.nodes.get(desIndex);
        }

        // add the edge to nodes' edges list
        srcNode.edges.put(desNode, newEdge);
        desNode.edges.put(srcNode, newEdge);
    }

    private static void printGraph(Graph graph) {
        List<Edge> reachedEdges = new ArrayList<>();
        graph.nodes.forEach(node -> {
            node.edges.forEach((node1, edge) -> {

                // prints edge only once
                if (!reachedEdges.contains(edge)) {
                    System.out.println(node.label + " -- " + edge.weight + " -- " + node1.label);
                    reachedEdges.add(edge);
                }
            });
        });
        System.out.println("-- -- -- -- --");
    }

    private static Node[] prim() {
        PriorityQueue<Node> queue = new PriorityQueue<Node>();
        int key[] = new int[numOfNodes];
        Node parent[] = new Node[numOfNodes];

        // init queue and keys array
        for (Node node : graph.nodes){
            graphMST.nodes.add(node);
            queue.add(node);
            key[node.label] = Integer.MAX_VALUE;
        };

        // start from the first node
        key[0] = 0;
        parent[0] = null;

        while (!queue.isEmpty()) {
            Node u = queue.poll();

            for (Map.Entry<Node, Edge> entry : u.edges.entrySet()) {
                if (queue.contains(entry.getKey()) && entry.getValue().weight < key[entry.getKey().label]) {
                    parent[entry.getKey().label] = u;
                    key[entry.getKey().label] = entry.getValue().weight;
                }
            }
            
            // create the graphMST
            // Node srcNode = graphMST.nodes.get(srcIndex);
            // Node desNode = graphMST.nodes.get(desIndex);
            // // add the edge to nodes' edges list
            // srcNode.edges.put(desNode, newEdge);
            // desNode.edges.put(srcNode, newEdge);
        }
        return parent;
    }
    
    private static void printMST(Node[] nodes) {
        for(Node node: nodes) {
        }
    }


}
